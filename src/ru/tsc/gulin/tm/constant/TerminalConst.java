package ru.tsc.gulin.tm.constant;

public final class TerminalConst {

    public static final String ABOUT = "about";

    public static final String VERSION = "version";

    public static final String HELP = "help";
}
